# Requirement

* `eslint`
* `eslint-plugin-promise`
* `eslint-plugin-lodash`
* `eslint-plugin-lodash-fp`
* `prettier`

# Usage

Install the package (and all the requirements, if you haven't) first.

```
npm i prettier eslint{,-config-bentinata,-plugin-{promise,lodash{,-fp}}} -D
```

Put these on your `eslintrc.json` or `eslintConfig` properties:

```
"extends": "bentinata"
```

# Caveat

On v2 onwards, I've been integrating `prettier`.
My approach is overriding the `eslint` in `node_modules/.bin`.
Since `prettier` don't accept `.` as arguments,
I'm _prettifying_ all `*.js` file, except `*.min.js`.
If you have a better approach, please do a merge request, or notify me.

# Comparison

It's always good to see comparison.
You can look to [diff table](difference.md).

* With ESLint recommendation
  ESLint ship with recommended rules. It's got many friction with ours though.
* With AirBnB
  AirBnB JavaScript style is a popular one. Our style also originated from there.
